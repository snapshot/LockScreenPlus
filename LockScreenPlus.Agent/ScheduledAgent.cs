﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using System;

namespace LockScreenPlus.Agent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            //TODO: Add code to perform your task in background
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                var listfiles = myIsolatedStorage.GetFileNames("*.jpg.full");
                List<string> SelectedFiles = new List<string>();

                foreach (var item in listfiles)
                {
                    SelectedFiles.Add(item);
                }

                int HowMany = SelectedFiles.Count;
                if (HowMany > 0)
                {
                    Random random = new Random();
                    int num = random.Next(HowMany);

                    var result = Models.PictureLocker.LockHelper(SelectedFiles[num], false);
                }

            }
            NotifyComplete();
        }
    }
}