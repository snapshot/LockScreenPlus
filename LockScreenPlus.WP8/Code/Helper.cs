﻿using Coding4Fun.Phone.Controls;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LockScreenPlus.WP8.Code
{
    public class Helper
    {
        private const string AppManifestName = "WMAppManifest.xml";
        private const string AppNodeName = "App";
        private const string AppProductIDAttributeName = "ProductID";
        private const string AppVersionAttributeName = "Version";

        public static string BuildApplicationDeepLink()
        {
            var applicationId = Guid.Parse(GetManifestAttributeValue(AppProductIDAttributeName));

            return BuildApplicationDeepLink(applicationId.ToString());
        }

        public static string GetAppVersion()
        {
            return GetManifestAttributeValue(AppVersionAttributeName);
        }

        public static string BuildApplicationDeepLink(string applicationId)
        {
            return @"http://windowsphone.com/s?appid=" + applicationId;
        }

        public static string GetManifestAttributeValue(string attributeName)
        {
            var xmlReaderSettings = new XmlReaderSettings
            {
                XmlResolver = new XmlXapResolver()
            };

            using (var xmlReader = XmlReader.Create(AppManifestName, xmlReaderSettings))
            {
                xmlReader.ReadToDescendant(AppNodeName);

                if (!xmlReader.IsStartElement())
                {
                    throw new FormatException(AppManifestName + " is missing " + AppNodeName);
                }

                return xmlReader.GetAttribute(attributeName);
            }
        }


        public static string CreateUrl(string destination, Dictionary<string, string> Params)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append(destination);
            sb.Append("?");
            foreach (var item in Params)
            {
                sb.Append(item.Key);
                sb.Append("=");
                sb.Append(item.Value);
                sb.Append("&");

            }
            return sb.ToString();

        }

        public static void ShowToast(string StatusMessage, int Code)
        {
            ShowToast(string.Format("[{0}] {1}", Code, StatusMessage));
        }

        public static void ShowToast(string Mgs)
        {

            ToastPrompt toast = new ToastPrompt();
            toast.FontSize = 20;
            toast.Title = "LOCKSCREENPLUS";
            toast.Message = Mgs;
            toast.TextOrientation = System.Windows.Controls.Orientation.Horizontal;
            toast.Show();
        }

        public static void OpenBrowser(string url)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask { Uri = new Uri(url, UriKind.RelativeOrAbsolute) };
            webBrowserTask.Show();
        }

        public static void MailTo(string url)
        {
            string[] m = url.Split(':');

            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.To = m[1];

            emailComposeTask.Show();
        }
    }
}
