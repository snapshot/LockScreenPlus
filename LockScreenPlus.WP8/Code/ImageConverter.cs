﻿using Microsoft.Phone;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace LockScreenPlus.WP8.Code
{
    public class ImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            BitmapImage bi = new BitmapImage();

            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile(value.ToString(), FileMode.Open, FileAccess.Read))
                {
                    bi.SetSource(fileStream);
                  // return PictureDecoder.DecodeJpeg(fileStream);
                    fileStream.Close();
                }
            }
            return bi;

            //string name = value.ToString();

            //return new Uri(string.Format("isostore://{0}", name), UriKind.Absolute);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
