﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LockScreenPlus.WP8.ViewModels
{
    public class ImageDetails : BaseViewModel
    {

        private string _Image;

        public string Image
        {
            get { return _Image; }
            set { _Image = value; NotifyPropertyChanged(); }
        }


        async public void SetImage()
        {
            await LockScreenPlus.Models.PictureLocker.LockHelper(Image.Replace("thumb", "full"), false);

            Code.Helper.ShowToast("image set");
        }


        async public Task RotateImage(bool right)
        {
            ++LoadingCounter;
            string oginigal = Image;
            await Rotate(right);
            Image = oginigal;
            --LoadingCounter;
        }

        async private Task<bool> Rotate(bool right)
        {

            return await Task.Run(() =>
                     {
                         return RotateNormal(right, Image);
                     });
        }

        public static bool RotateNormal(bool right, string image)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {

                            string newName = image;
                            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                            {

                                IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile(image, FileMode.Open, FileAccess.Read);

                                BitmapImage bitmap = new BitmapImage();
                                bitmap.SetSource(fileStream);
                                fileStream.Close();
                                myIsolatedStorage.DeleteFile(image);

                                WriteableBitmap wb = new WriteableBitmap(bitmap);
                                if (right)
                                {

                                    wb = wb.RotateFree(-180);
                                }
                                else
                                    wb = wb.RotateFree(180);


                                IsolatedStorageFileStream fileStreamS = myIsolatedStorage.CreateFile(image);

                                Extensions.SaveJpeg(wb, fileStreamS, wb.PixelWidth, wb.PixelHeight, 0, 100);
                                fileStreamS.Close();




                                fileStream = myIsolatedStorage.OpenFile(image.Replace("thumb", "full"), FileMode.Open, FileAccess.Read);

                                bitmap = new BitmapImage();
                                bitmap.SetSource(fileStream);
                                fileStream.Close();
                                myIsolatedStorage.DeleteFile(image.Replace("thumb", "full"));

                                wb = new WriteableBitmap(bitmap);
                                if (right)
                                {
                                    wb = wb.RotateFree(-180);
                                }
                                else
                                    wb = wb.RotateFree(180);

                                fileStreamS = myIsolatedStorage.CreateFile(image.Replace("thumb", "full"));

                                Extensions.SaveJpeg(wb, fileStreamS, wb.PixelWidth, wb.PixelHeight, 0, 100);
                                fileStreamS.Close();

                                image = newName;

                            }

                        }); return true;
        }
    }
}
