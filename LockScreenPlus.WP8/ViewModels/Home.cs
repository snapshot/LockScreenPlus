﻿using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using LockScreenPlus.WP8.Code;

namespace LockScreenPlus.WP8.ViewModels
{
    public class Home : BaseViewModel
    {

        private ObservableCollection<LockScreenPlus.Models.PictureStorage> _photoList = new ObservableCollection<LockScreenPlus.Models.PictureStorage>();

        public ObservableCollection<LockScreenPlus.Models.PictureStorage> PhotoList
        {
            get { return _photoList; }
            set
            {
                _photoList = value;
                NotifyPropertyChanged();
            }
        }


        public void Alerts()
        {

            var settings = IsolatedStorageSettings.ApplicationSettings;
            var pushsett = settings.Any(o => o.Key == UIConstants.SETTINGS.VOTE_FOR_ME);


            if (!pushsett)
            {

                DateTime now = DateTime.Now;
                DateTime endPromo = new DateTime(2013, 1, 8);
                int result = DateTime.Compare(endPromo, now);

                if (result > 0)
                {
                    var r = MessageBox.Show("Support US by voting in the Lumia Geek 8-Week Competition", "LockScreenPlus", MessageBoxButton.OKCancel);
                    if (r == MessageBoxResult.OK)
                    {
                        if (settings.Contains(UIConstants.SETTINGS.VOTE_FOR_ME))
                        {
                            settings[UIConstants.SETTINGS.VOTE_FOR_ME] = true;
                        }
                        else
                        {
                            settings.Add(UIConstants.SETTINGS.VOTE_FOR_ME, true);
                        }
                        Helper.OpenBrowser("http://windowsphonegeek.com/lumia-geek-8-week-challenge/lockscreenplus");
                    }
                }
                else
                {

                }
            }
        }

        private bool isTask;

        public bool IsTask
        {
            get { return isTask; }
            set
            {
                isTask = value; NotifyPropertyChanged();
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                settings["BACKGROUND_TASK"] = value;
                SetAgent(settings, value);
            }
        }


        async public void FisrtLoad()
        {
            var isProvider = Windows.Phone.System.UserProfile.LockScreenManager.IsProvidedByCurrentApplication;
            if (!isProvider)
            {
                // If you're not the provider, this call will prompt the user for permission.
                // Calling RequestAccessAsync from a background agent is not allowed.
                var op = await Windows.Phone.System.UserProfile.LockScreenManager.RequestAccessAsync();

                // Only do further work if the access was granted.
                isProvider = op == Windows.Phone.System.UserProfile.LockScreenRequestResult.Granted;
            }

        }

        public void SetAgent(IsolatedStorageSettings settings, bool IsTaskEnabled)
        {
            PeriodicTask periodicTask = new PeriodicTask("AGENT_BACKGROUND_TASK");

            periodicTask.Description = "LockScreenPlus enables the automatic change your lockscreen image from the chosen list";
            periodicTask.ExpirationTime = System.DateTime.Now.AddDays(10);

            if (ScheduledActionService.Find(periodicTask.Name) != null)
            {
                ScheduledActionService.Remove("AGENT_BACKGROUND_TASK");
            }

            if (IsTaskEnabled)
            {
                try
                {

                    ScheduledActionService.Add(periodicTask);
                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        ScheduledActionService.LaunchForTest("AGENT_BACKGROUND_TASK", TimeSpan.FromMilliseconds(3500));
                    }

                }
                catch (InvalidOperationException ex)
                {
                    if (ex.Message.Contains("BNS Error: The action is disabled"))
                    {
                        MessageBox.Show("Background agents for this application have been disabled by the user.");
                        IsTask = false;

                    }

                }
            }
        }

        public void Delete(List<Models.PictureStorage> toDelete)
        {

            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {

                foreach (var item in toDelete)
                {
                    myIsolatedStorage.DeleteFile(item.Name);
                    myIsolatedStorage.DeleteFile(item.Name.Replace("thumb", "full"));
                }
                var listfiles = myIsolatedStorage.GetFileNames("*.jpg.full");

                if (listfiles.Count() > 0)
                    Models.PictureLocker.LockHelper(listfiles[0], false);

                Code.Helper.ShowToast("images deleted");
            }


        }

        async public Task Rotate(List<Models.PictureStorage> toRotate, bool right)
        {

            ++LoadingCounter;
            foreach (var item in toRotate)
            {
                await Task.Run(() =>
                      {
                          ImageDetails.RotateNormal(right, item.Name);
                      });
            }

            --LoadingCounter;
        }

        async public Task Load()
        {
            FisrtLoad();
            PhotoList.Clear();
            await GetPhotosFromIO();
            Alerts();
        }

        async private Task GetPhotosFromIO()
        {
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                var listfiles = myIsolatedStorage.GetFileNames("*.jpg.thumb");

                foreach (var item in listfiles)
                {
                    PhotoList.Add(new LockScreenPlus.Models.PictureStorage { Name = item });
                }

            }
        }

        public ObservableCollection<BitmapImage> MyPhotos { get; set; }

        public void AddPicture(bool resize)
        {
            PhotoChooserTask selectphoto = new PhotoChooserTask();
            if (resize)
                switch (App.Current.Host.Content.ScaleFactor)
                {
                    case 100:
                        selectphoto.PixelHeight = 800;
                        selectphoto.PixelWidth = 480;
                        break;
                    case 150:
                        selectphoto.PixelHeight = 1280;
                        selectphoto.PixelWidth = 720;
                        break;
                    case 160:
                        selectphoto.PixelHeight = 1280;
                        selectphoto.PixelWidth = 768;
                        break;
                    default:
                        break;
                }

            selectphoto.Completed += new EventHandler<PhotoResult>(selectphoto_Completed);
            selectphoto.Show();

        }

        async public void OpenSettings()
        {

            var op = await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-lock:"));
        }

        private void selectphoto_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {

                SaveImage(e.ChosenPhoto, e.OriginalFileName.Substring(e.OriginalFileName.LastIndexOf("\\") + 1));

            }

        }


        private string RemoveSpecialCharacters(string s)
        {
            return Regex.Replace(s, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }

        public void SaveImage(Stream e, string fileName)
        {

            BinaryReader reader = new BinaryReader(e);

            fileName = fileName.Replace("http://", "").Replace("/", "").Replace("dat", "jpg");
            fileName = RemoveSpecialCharacters(fileName);
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (myIsolatedStorage.FileExists(fileName))
                {
                    myIsolatedStorage.DeleteFile(fileName);
                }

                IsolatedStorageFileStream fileStream = myIsolatedStorage.CreateFile(fileName + ".full");

                IsolatedStorageFileStream fileStream2 = myIsolatedStorage.CreateFile(fileName + ".thumb");

                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.SetSource(e);
                    WriteableBitmap wb = new WriteableBitmap(bitmap);
                    //  WriteableBitmap wbThumn = new WriteableBitmap(bitmap);
                    /*resize*/

                    int CurrentWidth = wb.PixelWidth;
                    int CurrentHeight = wb.PixelHeight;

                    if (CurrentWidth > CurrentHeight)
                    {


                        //     wbThumn = wbThumn.Rotate(90);

                        wb = wb.Rotate(90);
                    }
                    else
                    {


                    }


                    //   wbThumn = wbThumn.Resize(wbThumn.PixelWidth / 4, wbThumn.PixelHeight / 4, WriteableBitmapExtensions.Interpolation.Bilinear);



                    int factor = wb.PixelWidth / 120;

                    if (factor > 4) factor = 4;

                    Extensions.SaveJpeg(wb.Clone().Resize(wb.PixelWidth / factor, wb.PixelHeight / factor, WriteableBitmapExtensions.Interpolation.Bilinear)

                        , fileStream2, wb.PixelWidth / factor, wb.PixelHeight / factor, 0, 100);

                    factor = 1;
                    if (wb.PixelWidth > 768)
                    {
                        factor = wb.PixelWidth / 768;
                    }

                    //   Extensions.SaveJpeg(wb, fileStream, wb.PixelWidth, wb.PixelHeight, 0, 100);


                    Extensions.SaveJpeg(wb.Clone().Resize(wb.PixelWidth / factor, wb.PixelHeight / factor, WriteableBitmapExtensions.Interpolation.Bilinear)

                        , fileStream, wb.PixelWidth / factor, wb.PixelHeight / factor, 0, 100);


                }
                catch (Exception)
                {

                    throw;
                }

                Code.Helper.ShowToast("image added");

                fileStream.Close();
                fileStream2.Close();


                //LockScreenPlus.Models.PictureStorage pc = new Models.PictureStorage();

                //pc.LockHelper(fileName, false);
            }
        }
    }
}
