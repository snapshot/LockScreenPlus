﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Tasks;
using LockScreenPlus.WP8.Code;
using Microsoft.Xna.Framework.Media;
using System.Windows.Media.Imaging;

namespace LockScreenPlus.WP8
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor

        ViewModels.Home VM = new ViewModels.Home();

        ApplicationBarIconButton picturesEnableSelect;
        ApplicationBarIconButton addPictures;
        ApplicationBarIconButton addPlusPictures;
        ApplicationBarIconButton removePictures;
        ApplicationBarIconButton pictureDisableSelect;
        ApplicationBarIconButton pictureRotateL;
        ApplicationBarIconButton pictureRotateR;
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = VM;
            CreateMenu();
            StartAgent();
         
        }


      

        private void AppBarSetButtons(bool b)
        {

            for (int i = 0; i < ApplicationBar.Buttons.Count; i++)
            {
                ((ApplicationBarIconButton)this.ApplicationBar.Buttons[i]).IsEnabled = b;
            }
        }

        private void CreateMenu()
        {

            picturesEnableSelect = new ApplicationBarIconButton();
            picturesEnableSelect.IconUri = new Uri("/Toolkit.Content/ApplicationBar.Select.png", UriKind.RelativeOrAbsolute);
            picturesEnableSelect.Text = "select";
            picturesEnableSelect.Click += new EventHandler((sender, e) =>
            {

                PhotoGrid.EnforceIsSelectionEnabled = true;
                SelectionMenu();
            });


            removePictures = new ApplicationBarIconButton();
            removePictures.IconUri = new Uri("/Toolkit.Content/ApplicationBar.Delete.png", UriKind.RelativeOrAbsolute);
            removePictures.Text = "delete";
            removePictures.Click += new EventHandler((sender, e) =>
            {
                if (PhotoGrid.SelectedItems.Count > 0)
                {
                    var toDelete = PhotoGrid.SelectedItems;
                    List<Models.PictureStorage> M = new List<Models.PictureStorage>();
                    foreach (var item in toDelete)
                    {
                        M.Add((Models.PictureStorage)item);
                    }


                    VM.Delete(M);
                    NormalMenu();
                    PhotoGrid.EnforceIsSelectionEnabled = false;
                    VM.Load();
                }
            });

            pictureDisableSelect = new ApplicationBarIconButton();
            pictureDisableSelect.IconUri = new Uri("/Toolkit.Content/ApplicationBar.Cancel.png", UriKind.RelativeOrAbsolute);
            pictureDisableSelect.Text = "cancel";
            pictureDisableSelect.Click += new EventHandler((sender, e) =>
            {
                PhotoGrid.EnforceIsSelectionEnabled = false;
                NormalMenu();
            });


            addPictures = new ApplicationBarIconButton();
            addPictures.IconUri = new Uri("/Assets/AppBar/add.png", UriKind.RelativeOrAbsolute);
            addPictures.Text = "add";
            addPictures.Click += new EventHandler((sender, e) =>
            {
                VM.AddPicture(false);

            });

            addPlusPictures = new ApplicationBarIconButton();
            addPlusPictures.IconUri = new Uri("/Assets/AppBar/Fit-To-Size.png", UriKind.RelativeOrAbsolute);
            addPlusPictures.Text = "add";
            addPlusPictures.Click += new EventHandler((sender, e) =>
            {
                VM.AddPicture(true);

            });

            pictureRotateL = new ApplicationBarIconButton();
            pictureRotateL.IconUri = new Uri("/Assets/AppBar/rotatel.png", UriKind.RelativeOrAbsolute);
            pictureRotateL.Text = "left";
            pictureRotateL.Click += pictureRotateL_Click;


            pictureRotateR = new ApplicationBarIconButton();
            pictureRotateR.IconUri = new Uri("/Assets/AppBar/rotater.png", UriKind.RelativeOrAbsolute);
            pictureRotateR.Text = "right";
            pictureRotateR.Click += pictureRotateR_Click;




        }

        async void pictureRotateL_Click(object sender, EventArgs e)
        {
            var toDelete = PhotoGrid.SelectedItems;
            List<Models.PictureStorage> M = new List<Models.PictureStorage>();
            foreach (var item in toDelete)
            {
                M.Add((Models.PictureStorage)item);
            }
            AppBarSetButtons(false);
            await VM.Rotate(M, false);
            await VM.Load();
            AppBarSetButtons(true);
        }

        async void pictureRotateR_Click(object sender, EventArgs e)
        {
            var toDelete = PhotoGrid.SelectedItems;
            List<Models.PictureStorage> M = new List<Models.PictureStorage>();
            foreach (var item in toDelete)
            {
                M.Add((Models.PictureStorage)item);
            }
            AppBarSetButtons(false);
            await VM.Rotate(M, true);
            await VM.Load();
            AppBarSetButtons(true);
        }


        private void NormalMenu()
        {

            this.ApplicationBar.Buttons.Clear();
            this.ApplicationBar.Buttons.Add(addPictures);

            this.ApplicationBar.Buttons.Add(addPlusPictures); this.ApplicationBar.Buttons.Add(picturesEnableSelect);
        }

        private void SelectionMenu()
        {
            this.ApplicationBar.Buttons.Clear();
            this.ApplicationBar.Buttons.Add(pictureRotateL);
            this.ApplicationBar.Buttons.Add(removePictures);
            this.ApplicationBar.Buttons.Add(pictureDisableSelect);

            this.ApplicationBar.Buttons.Add(pictureRotateR);
        }

        // Load data for the ViewModel Items
        async protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);

            IDictionary<string, string> queryStrings = this.NavigationContext.QueryString;

            // Ensure that there is at least one key in the query string, and check whether the "token" key is present.
            if (queryStrings.ContainsKey("token"))
            {
                // Retrieve the photo from the media library using the token passed to the app.
                MediaLibrary library = new MediaLibrary();
                Picture photoFromLibrary = library.GetPictureFromToken(queryStrings["token"]);

                VM.SaveImage(photoFromLibrary.GetImage(), photoFromLibrary.Name);
            }



            NormalMenu();


            string lockscreenKey = "WallpaperSettings";
            string lockscreenValue = "0";

            bool lockscreenValueExists = NavigationContext.QueryString.TryGetValue(lockscreenKey, out lockscreenValue);

            if (lockscreenValueExists)
            {
                // Navigate the user to your app's lock screen settings screen here, 
                // or indicate that the lock screen background image is updating.
            }

            await VM.Load();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            VM.PhotoList.Clear();
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {

        }

        private void btnGoToLockSettings_Click(object sender, RoutedEventArgs e)
        {
            // Launch URI for the lock screen settings screen.

        }

        private void ApplicationBarIconButton_Click_2(object sender, EventArgs e)
        {
            VM.OpenSettings();
        }

        private void OnPictureItemTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Grid g = (Grid)sender;


            if (!PhotoGrid.EnforceIsSelectionEnabled)
                NavigationService.Navigate(new Uri("/ImageDetails.xaml?image=" + g.Tag.ToString(), UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBarIconButton_Click_3(object sender, EventArgs e)
        {



        }


        private void StartAgent()
        {
            //start background agent 
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            bool IsTaskEnabled = false;
            if (settings.Contains("BACKGROUND_TASK"))
            {
                IsTaskEnabled = (bool)settings["BACKGROUND_TASK"];
            }
            else
            {
                IsTaskEnabled = true;
            }

            VM.IsTask = IsTaskEnabled;

        }

        private void Gmail_Click(object sender, RoutedEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.To = "cristovao.morgado@gmail.com";
            emailComposeTask.Subject = "LockScreenPlus";
            emailComposeTask.Show();
        }


        private void LinkedIn_Click(object sender, RoutedEventArgs e)
        {
            Helper.OpenBrowser("http://pt.linkedin.com/in/cmmorgado");
        }

        private void Twitter_Click(object sender, RoutedEventArgs e)
        {
            Helper.OpenBrowser("http://twitter.com/TheSaintr");
        }

        private void IFixit_Click(object sender, RoutedEventArgs e)
        {
            MarketplaceDetailTask task = new MarketplaceDetailTask();
            task.ContentIdentifier = "217f172d-de13-42cb-b2bc-a80928a64a9c";
            task.Show();
        }


    }
}