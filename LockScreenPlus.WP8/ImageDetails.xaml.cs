﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace LockScreenPlus.WP8
{
    public partial class ImageDetails : PhoneApplicationPage
    {

        ViewModels.ImageDetails VM = new ViewModels.ImageDetails();
        public ImageDetails()
        {
            InitializeComponent();

        }



        private void AppBarSetButtons(bool b)
        {

            for (int i = 0; i < ApplicationBar.Buttons.Count; i++)
            {
                ((ApplicationBarIconButton)this.ApplicationBar.Buttons[i]).IsEnabled = b;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);

            string Image = string.Empty;

            bool ImageParam = NavigationContext.QueryString.TryGetValue("image", out Image);

            if (ImageParam)
            {
                VM.Image = Image;
                DataContext = VM;
            }
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            VM.SetImage();
        }

       async private void ApplicationBarIconButton_Click_L(object sender, EventArgs e)
        {
            AppBarSetButtons(false);
            await VM.RotateImage(true);
            AppBarSetButtons(true);
        }

        async private void ApplicationBarIconButton_Click_R(object sender, EventArgs e)
        {
            AppBarSetButtons(false);
            await VM.RotateImage(false);
            AppBarSetButtons(true);
        }
    }
}